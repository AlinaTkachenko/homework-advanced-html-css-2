const gulp = require("gulp");
const {series,parallel} = require("gulp");
const concat = require("gulp-concat");
const gulpClean = require("gulp-clean");
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const minify = require('gulp-minify');
const  autoprefixer  =  require ( 'gulp-autoprefixer' );
const imagemin = require('gulp-imagemin');

const clean = function () {
    return gulp.src("dist/**/*.*")
        .pipe(gulpClean())
}

const minCss = function () {
    return gulp.src("src/scss/style.scss")
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        
        .pipe(autoprefixer()) 
        //.pipe(concat("styles.css"))
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest("dist/style"))
        .pipe(browserSync.stream());
}

const minJs = function () {
    return gulp.src("src/JS/*.js")
        .pipe(concat("script.js"))
        .pipe(minify())
        .pipe(gulp.dest("dist/JS/"))
        .pipe(browserSync.stream());
}

const minImg = function() {
    return gulp.src('src/img/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
        .pipe(browserSync.stream());
};

function watch() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("./index.html").on("change", browserSync.reload)
    gulp.watch("src/scss/*.scss", minCss)
    gulp.watch("src/JS/*.js", minJs)
    gulp.watch("src/img/*.*", minImg)
}

const browsersync = function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
}

gulp.task('browser-sync', browsersync);
gulp.task("clean", clean)
gulp.task("minCss", minCss)
gulp.task("minJs", minJs)
gulp.task("minImg", minImg)
gulp.task("watch", watch)

exports.build = series(clean, minCss, minJs, minImg)
exports.dev = series(clean, minCss, minJs, minImg, watch)