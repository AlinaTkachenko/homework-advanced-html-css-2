const navigationIcons = document.getElementById('nav-icon');
const menuIcon = document.querySelector('.menu');
const closeIcon = document.querySelector('.close');
const navigationList = document.querySelector('.navigation__list');

navigationIcons.addEventListener('click', function (event) {
    menuIcon.classList.toggle('invisible');
    closeIcon.classList.toggle('invisible');
    if (menuIcon.classList.contains('invisible')) {
        navigationList.style.display = "block"
    } else {
        navigationList.style.display = "none"
    }
});

navigationList.addEventListener('click', function (event) {
    const activeLink = document.querySelectorAll('.active_link');
    activeLink.forEach(function (elem) {
        elem.classList.remove('active_link');
    })
    event.target.classList.add('active_link');
})

console.log('hi')